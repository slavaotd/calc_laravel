<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Calculator</title>
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    </head>
    <body>
        <div class="container mt-5">
            <div class="row">
                <div class="col-12">
                    <a class="btn btn-primary btn-lg" href="{{ route('calc.index') }}">Go to calc!</a>
                </div>
            </div>
        </div>
    </body>
</html>
