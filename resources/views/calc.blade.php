<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">

        <title>Calc</title>
    </head>
    <body>

        <div class="container">
            <div class="row mt-5">
                <div class="col-12">
                    <h1>Calculate sum of two numbers</h1>
                </div>
            </div>

            <div class="row mt-5">
                <div class="col-12 col-lg-6">

                    <form action="{{ route('calc.store') }}" method="post">
                        @csrf
                        <div class="row">

                        </div>
                        <div class="form-group">
                            <label for="number1">Number 1</label>
                            <input id="number1"
                                   type="text"
                                   class="form-control{{ $errors->has('number1') ? ' is-invalid' : '' }}"
                                   name="number1"
                                   value="{{ old('number1') }}"
                                   autofocus>
                            @if ($errors->has('number1'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('number1') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="number2">Number 2</label>
                            <input id="number2"
                                   type="text"
                                   class="form-control{{ $errors->has('number2') ? ' is-invalid' : '' }}"
                                   name="number2"
                                   value="{{ old('number2') }}"
                                   autofocus>
                            @if ($errors->has('number2'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('number2') }}</strong>
                                </span>
                            @endif
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>

                </div>
            </div>
        </div>

    </body>
</html>
