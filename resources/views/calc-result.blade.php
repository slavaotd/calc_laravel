<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">

        <title>Calc</title>
    </head>
    <body>

        <div class="container">
            <div class="row mt-5">
                <div class="col-12">
                    <h1>Result of the sum of two numbers</h1>
                    <div class="h2">{{ $number1 }} + {{ $number2 }} = {{ $result }}</div>
                </div>
                <div class="col-12 mt-5">
                    <a class="btn btn-outline-primary" href="{{ route('calc.index') }}">Go back to calc</a>
                </div>
            </div>
        </div>

    </body>
</html>
