<?php

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Artisan::call('view:clear');

Route::get('/', function () {
    return view('welcome');
});

Route::get('/calc', 'CalcController@index')->name('calc.index');
Route::post('/calc', 'CalcController@store')->name('calc.store');
