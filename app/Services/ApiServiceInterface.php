<?php

namespace App\Services;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\RequestOptions;

/**
 * Interface ApiServiceInterface
 * @package App\Services
 */
interface ApiServiceInterface
{
    /**
     * ApiServiceInterface constructor.
     * @param ApiServiceResource $api_service_resource
     */
    public function __construct(ApiServiceResource $api_service_resource);

    /**
     * @param $method
     * @param $number1
     * @param $number2
     * @return ApiServiceResource
     */
    public function calculate($method, $number1, $number2): ApiServiceResource;
}
