<?php

namespace App\Services;


/**
 * Interface ApiServiceResourceInterface
 * @package App\Services
 */
interface ApiServiceResourceInterface
{
    /**
     * @return mixed
     */
    public function setNotValid();

    /**
     * @return bool
     */
    public function isValid(): bool;

    /**
     * @return mixed
     */
    public function getResult();
}
