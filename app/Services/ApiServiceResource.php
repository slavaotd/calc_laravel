<?php

namespace App\Services;


/**
 * Class ApiServiceResource
 * @package App\Services
 */
class ApiServiceResource implements ApiServiceResourceInterface
{
    public $is_valid = true;
    public $result = null;

    /**
     * ApiServiceResource constructor.
     */
    public function __construct() {
    }

    /**
     * Is response valid
     *
     * @return bool
     */
    public function isValid(): bool
    {
        return $this->is_valid;
    }

    /**
     * Get result
     *
     * @return null
     */
    public function getResult() {
        return $this->result;
    }

    /**
     *  Set response not valid
     */
    public function setNotValid() {
        $this->is_valid = false;
    }

    /**
     * Set result value
     *
     * @param $value
     */
    public function setResult($value) {
        $this->result = $value;
    }
}
