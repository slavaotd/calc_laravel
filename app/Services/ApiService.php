<?php

namespace App\Services;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\RequestOptions;

/**
 * Class ApiService
 * @package App\Services
 */
class ApiService implements ApiServiceInterface
{
    private $resource;
    private $protocol;
    private $host;

    /**
     * @param $method
     * @return false|string
     */
    private function getEndpoint($method) {
        switch ($method) {
            case 'sum': return $this->protocol .'://'. $this->host .'/api/calc-sum';
        }
        return false;
    }

    /**
     * ApiService constructor.
     * @param ApiServiceResource $api_service_resource
     */
    public function __construct(ApiServiceResource $api_service_resource) {
        $this->protocol = env('API_CALC_PROTOCOL');
        $this->host = env('API_CALC_HOST');
        $this->resource = $api_service_resource;
    }

    /**
     * Just calculate
     *
     * @param $method
     * @param $number1
     * @param $number2
     * @return ApiServiceResource
     * @throws GuzzleException
     */
    public function calculate($method, $number1, $number2): ApiServiceResource
    {
        switch ($method) {
            case 'sum': return $this->calcSum($number1, $number2, $this->getEndpoint($method));
        }
        return $this->resource;
    }

    /**
     * @param $number1
     * @param $number2
     * @param $endpoint
     * @return ApiServiceResource
     * @throws GuzzleException
     */
    public function calcSum($number1, $number2, $endpoint): ApiServiceResource
    {
        $response = (new Client())->post($endpoint, [
            RequestOptions::JSON => [
                'number1' => $number1,
                'number2' => $number2,
            ],
        ]);

        if ($response->getStatusCode() != 200) {
            $this->resource->setNotValid();
            return $this->resource;
        }

        $content = json_decode($response->getBody(), true);
        if (json_last_error() !== JSON_ERROR_NONE) {
            $this->resource->setNotValid();
            return $this->resource;
        }

        $this->resource->setResult($content['sum']);
        return $this->resource;
    }
}
