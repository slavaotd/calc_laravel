<?php

namespace App\Http\Controllers;

use App\Services\ApiService;
use App\Services\ApiServiceResource;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Illuminate\View\View;

/**
 * Class CalcController
 * @package App\Http\Controllers
 */
class CalcController extends Controller
{
    /**
     * @return Application|Factory|View
     */
    public function index() {
        return view('calc');
    }

    /**
     * Store form post with two numbers for sum of them
     *
     * @return Application|RedirectResponse|Redirector
     * @throws GuzzleException
     */
    public function store()
    {
        $data = request()->validate([
            'number1' => ['required', 'numeric'],
            'number2' => ['required', 'numeric'],
        ]);

        $api_service_resource = (new ApiService(new ApiServiceResource()))->calculate('sum', $data['number1'], $data['number2']);

        if (!$api_service_resource->isValid()) {
            return view('calc-error');
        }

        return view('calc-result', [
            'number1' => $data['number1'],
            'number2' => $data['number2'],
            'result' => $api_service_resource->getResult(),
        ]);
    }
}
